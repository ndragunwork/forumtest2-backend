#ENV DEV
```
HOST=localhost
PORT=3333
NODE_ENV=development
APP_NAME=AdonisJs
APP_URL=http://localhost:3333
CACHE_VIEWS=false
APP_KEY=7KzBaXECXLdb7pZ4KIo973ji5KCiR7lH
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER=root
DB_PASSWORD=
DB_DATABASE=adonis-forum
HASH_DRIVER=bcrypt

######################
# SOCIAL AUTH DATA #
######################
FB_REDIRECT_URL=http://localhost:3000/?socialAuthSource=facebook
FB_CLIENT_ID=500379227091876
FB_CLIENT_SECRET=0380a65a82660edf8123aeddcdf065c4
TWITTER_REDIRECT_URL=http://localhost:3000/
TWITTER_ID=
TWITTER_SECRET=
GOOGLE_REDIRECT_URL=http://localhost:3000/?socialAuthSource=facebook
GOOGLE_CLIENT_ID=19284606384-9vnc2nnca7je4crf4ngoc7ubl7e6k6nk.apps.googleusercontent.com
GOOGLE_CLIENT_SECRET=MRwmS1ZCoZESqar3D0ad0SlY
LINKEDIN_REDIRECT_URL=http://localhost:3000/
LINKEDIN_ID=
LINKEDIN_SECRET=

# MAIL
MAIL_CONNECTION=mailgun
SEND_EMAIL_FROM=Admin <no-reply@posao.hr>
# IF SMTP #
MAIL_USERNAME=
MAIL_PASSWORD=
# IF MAILGUN #
MAILGUN_DOMAIN=
MAILGUN_API_KEY=
```


# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
